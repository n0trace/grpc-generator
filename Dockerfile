FROM golang:1.13 AS builder

WORKDIR /usr/src/grpc-generator

COPY . .

RUN apt update &&\
    apt install gcc g++ make automake autoconf libtool -y &&\
    make install-plugins

